#include <iostream>
#include <cstdint>
#include <cmath>

const std::string TASK_SEPARATOR = "========================================================";
/**
 * Print separator between tasks.
 */
void PrintSeparator() {
  std::cout << TASK_SEPARATOR << std::endl;
}

/**
 * Warning to show messages.
 */
class Warning : public std::exception
{
 public:
  explicit Warning(const std::string& msg) {
    std::cout << msg << std::endl;
  }
};

/**
 * Class to calculate powers.
 */
class Power {
  float a = 0;
  float b = 0;
 public:
  /**
   * Set values.
   *
   * @param a
   * First value.
   * @param b
   * Second value.
   * @return
   * self
   */
  Power* set(float a, float b) {
    this->a = a;
    this->b = b;
    return this;
  }
  /**
   * Calculate power value.
   *
   * @return
   * Power value.
   */
  [[nodiscard]] float calculate() const {
    return powf(a, b);
  }
};

/**
 * RGBA class.
 */
class RGBA {
  std::uint8_t m_red = 0;
  std::uint8_t m_green = 0;
  std::uint8_t m_blue = 0;
  std::uint8_t m_alpha = 255;
  /**
   * Helper function to print one parameter.
   *
   * @param name
   * Name of parameter.
   * @param value
   * Value of parameter.
   * @return
   * self
   */
  RGBA* print(const std::string& name, std::uint8_t value) {
    std::cout << name << " equal " << int(value) << std::endl;
    return this;
  }
 public:
  RGBA(std::uint8_t r, std::uint8_t g, std::uint8_t b, std::uint8_t a): m_red(r), m_green(g), m_blue(b), m_alpha(a) {}
  /**
   * Print all parameters.
   * @return
   * self
   */
  RGBA* print() {
    this->print("m_red", m_red)
    ->print("m_green", m_green)
    ->print("m_blue", m_blue)
    ->print("m_alpha", m_alpha);
    return this;
  }
};

/**
 * Stack class.
 */
class Stack {
  const int STACK_SIZE = 10;
  int* values = new int[STACK_SIZE];
  int size;
 public:
  Stack() {
    reset();
  }
  ~Stack() {
    delete[] values;
  }
  /**
   * Clear the stack.
   *
   * @return
   * self
   */
  Stack* reset() {
    for (int i = 0; i < STACK_SIZE; i++) {
      values[i] = 0;
    }
    size = -1;
    return this;
  }
  /**
   * Push value to stack.
   *
   * @param v
   * Value to push.
   * @return
   * True if not overfilled.
   */
  bool push(int v) {
    if (size < STACK_SIZE) {
      values[++size] = v;
      return true;
    }
    return false;
  }
  /**
   * Return value from stack.
   *
   * @return
   * Value.
   * @throws Warning
   */
  int pop() {
    if (size >= 0) {
      return values[size--];
    }
    throw Warning("Stack is empty");
  }
  Stack* print() {
    std::cout << "Stack start" << std::endl;
    for (int i = 0; i <= size; i++) {
      std::cout << "Value of stack with index " << i << " equals " << values[i] << std::endl;
    }
    std::cout << "Stack end" << std::endl;
    std::cout << std::endl;
    return this;
  }
};

int main() {
  /**
   * Task 1
   * Создать класс Power, который содержит два вещественных числа.
   * Этот класс должен иметь две переменные-члена для хранения этих вещественных чисел.
   * Еще создать два метода: один с именем set, который позволит присваивать значения переменным,
   * второй — calculate, который будет выводить результат возведения первого числа в степень второго числа.
   * Задать значения этих двух чисел по умолчанию.
   */
  {
    const float a = 3.2f;
    const float b = 2.1f;
    auto p = new Power();
    float result = p->set(a, b)
        ->calculate();
    std::cout << "Pow of " << a << " to " << b << " equals " << result << std::endl;
    delete p;
  }
  PrintSeparator();
  /**
   * Task 2
   * Написать класс с именем RGBA, который содержит 4 переменные-члена
   * типа std::uint8_t: m_red, m_green, m_blue и m_alpha (#include cstdint для доступа к этому типу).
   * Задать 0 в качестве значения по умолчанию для m_red, m_green, m_blue и 255 для m_alpha.
   * Создать конструктор со списком инициализации членов, который позволит пользователю
   * передавать значения для m_red, m_blue, m_green и m_alpha. Написать функцию print(),
   * которая будет выводить значения переменных-членов.
   */
  {
    RGBA* rgba = new RGBA(1, 2, 3, 4);
    rgba->print();
    delete rgba;
  }
  PrintSeparator();
  /**
   * Task 3
   * Написать класс, который реализует функциональность стека. Класс Stack должен иметь:
   * - private-массив целых чисел длиной 10;
   * - private целочисленное значение для отслеживания длины стека;
   * - public-метод с именем reset(), который будет сбрасывать длину и все значения элементов на 0;
   * - public-метод с именем push(), который будет добавлять значение в стек. push() должен возвращать значение false, если массив уже заполнен, и true в противном случае;
   * - public-метод с именем pop() для вытягивания и возврата значения из стека. Если в стеке нет значений, то должно выводиться предупреждение;
   * - public-метод с именем print(), который будет выводить все значения стека.
   */
  {
    Stack stack;
    stack.reset();
    stack.print();

    stack.push(3);
    stack.push(7);
    stack.push(5);
    stack.print();

    stack.pop();
    stack.print();

    stack.pop();
    stack.pop();
    stack.print();
  }

  return 0;
}
